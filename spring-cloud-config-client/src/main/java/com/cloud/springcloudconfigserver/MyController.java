package com.cloud.springcloudconfigserver;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MyController {
    @Value("${my.pushkar}")
    private String message;

    @Value("${pulkit}")
    private List<String> pulkit;

    @GetMapping("/hello")
    public String hello() {
        return message+pulkit;
    }

}
